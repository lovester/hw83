const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Album.find().populate({
        path: 'artist',
        match: { _id: req.query.artists},
    })
        .then(albums => res.send(albums))
        .catch(() => res.sendStatus(404));
});

router.get('/:id', (req, res) => {
    Album.findById(req.params.id)
        .then(result => {
            if (result) return res.send(result);
            res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('img'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
        albumData.img = req.file.filename;
    }

    const album = new Album(albumData);

    album.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;