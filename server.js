const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const config = require('./config');


const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const users = require('./app/users');

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/artists', artists);
    app.use('/albums', albums);
    app.use('/tracks', tracks);
    app.use('/users', users);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});
